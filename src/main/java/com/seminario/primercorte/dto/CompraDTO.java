package com.seminario.primercorte.dto;

import java.util.Date;

public class CompraDTO {

    private int idcompra;
    private String detalleCompra;
    private double valorCompra;
    private int cuotasCompra;
    private int idTarjeta;
    private int idUsuario;
    private java.util.Date fechaCompra;

    public int getIdcompra() {
        return idcompra;
    }

    public void setIdcompra(int idcompra) {
        this.idcompra = idcompra;
    }

    public String getDetalleCompra() {
        return detalleCompra;
    }

    public void setDetalleCompra(String detalleCompra) {
        this.detalleCompra = detalleCompra;
    }

    public double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public int getCuotasCompra() {
        return cuotasCompra;
    }

    public void setCuotasCompra(int cuotasCompra) {
        this.cuotasCompra = cuotasCompra;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(int idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
}
