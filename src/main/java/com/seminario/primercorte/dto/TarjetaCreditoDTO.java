package com.seminario.primercorte.dto;

public class TarjetaCreditoDTO {

    private int idtarjeta;
    private String nombre_alias;
    private double cupo;
    private double tasa_interes;
    private int fecha_corte;
    private String banco;
	public int getIdtarjeta() {
		return idtarjeta;
	}
	public void setIdtarjeta(int idtarjeta) {
		this.idtarjeta = idtarjeta;
	}
	public String getNombre_alias() {
		return nombre_alias;
	}
	public void setNombre_alias(String nombre_alias) {
		this.nombre_alias = nombre_alias;
	}
	public double getCupo() {
		return cupo;
	}
	public void setCupo(double cupo) {
		this.cupo = cupo;
	}
	public double getTasa_interes() {
		return tasa_interes;
	}
	public void setTasa_interes(double tasa_interes) {
		this.tasa_interes = tasa_interes;
	}
	public int getFecha_corte() {
		return fecha_corte;
	}
	public void setFecha_corte(int fecha_corte) {
		this.fecha_corte = fecha_corte;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}

}
