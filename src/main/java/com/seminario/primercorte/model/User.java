package com.seminario.primercorte.model;
import javax.persistence.*;

@Entity
@Table(name = "usuario")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idusuario;
	@Column(name = "nombre", length = 100)
	private String nombre;
	@Column(name = "apellidos", length = 100)
	private String apellidos;
	@Column(name = "identificacion", length = 100)
	private int identificacion;
	@Column(name = "correo", length = 100)
	private String correo;
	@Column(name = "contrasenia", length = 100)
	private String contrasenia;

	public int getIdusuario() {
	return idusuario;
}
	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public int getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(int identificacion) {
		this.identificacion = identificacion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	
	//este fue un cambio por 

}

