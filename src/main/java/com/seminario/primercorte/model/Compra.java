package com.seminario.primercorte.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
    @Table(name = "compra")
    public class Compra {


    @Id
    private int idcompra;   
    @Column(name = "detallecompra", length = 100)
    private String detalleCompra;    
    @Column(name = "valor_compra", length = 100)
    private double valorCompra;
    @Column(name = "cuotas_compra", length = 100)
    private int cuotasCompra;
    @Column(name = "idtarjeta", length = 100)
    private int idTarjeta;
    @Column(name = "idusuario", length = 100)
    private int idUsuario;
    @Column(name = "fecha_compra", length = 100)
    private java.util.Date fechaCompra;

    public void setIdCompra(int idCompra) {
		this.idcompra = idCompra;
	}
	public int getIdCompra() {
		return idcompra;
    }
    public void setDetalleCompra(String detalleCompra) {
		this.detalleCompra = detalleCompra;
	}
	public String getDetalleCompra() {
		return detalleCompra;
    }
    public void setValorCompra(Double valorCompra) {
		this.valorCompra = valorCompra;
	}
	public double getValorCompra() {
		return valorCompra;
    }
    public void setCuotasCompra(int cuotasCompra) {
		this.cuotasCompra = cuotasCompra;
	}
	public int getCuotasCompra() {
		return cuotasCompra;
    }
    public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}
	public int getIdTarjeta() {
		return idTarjeta;
    }
    public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdUsuario() {
		return idUsuario;
    }
    public void setFechaCompra(java.util.Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public java.util.Date getFechaCompra() {
		return fechaCompra;
	}




    }
