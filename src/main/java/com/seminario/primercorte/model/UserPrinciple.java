package com.seminario.primercorte.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class UserPrinciple implements UserDetails {

    private static final long serialVerionUID = 1L;
    private int idusuario;
    private String nombre;
    private String apellidos;
    private int identificacion;
    private String correo;

    @JsonIgnore
    private String contrasenia;

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrinciple(int idusuario, String nombre, String apellidos, int identificacion, String correo, String contrasenia, Collection<? extends GrantedAuthority> authorities) {
        this.idusuario = idusuario;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.identificacion = identificacion;
        this.correo = correo;
        this.contrasenia = contrasenia;
        this.authorities = authorities;
    }

    public static UserPrinciple build(User user) {

        return new UserPrinciple(
                user.getIdusuario(),
                user.getNombre(),
                user.getApellidos(),
                user.getIdentificacion(),
                user.getCorreo(),
                user.getContrasenia(),
                new ArrayList<>()
        );
    }

    public int getIdusuario() {
        return idusuario;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return contrasenia;
    }

    @Override
    public String getUsername() {
        return correo;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserPrinciple user = (UserPrinciple) o;
        return Objects.equals(idusuario, user.idusuario);
    }

    @Override
    public native int hashCode();
}
