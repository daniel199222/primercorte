package com.seminario.primercorte.model;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Esta clase es requerido para almacenar el usuario y la contrasela que sera recibida desde el cliente
 */
public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 5926468583005150707L;

    @ApiModelProperty(notes = "Username")
    private String username;

    @ApiModelProperty(notes = "Password")
    private String password;

    public JwtRequest(){
    }

    public JwtRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return username;
    }

    private void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    private void setPassword(String password) {
        this.password = password;
    }
}
