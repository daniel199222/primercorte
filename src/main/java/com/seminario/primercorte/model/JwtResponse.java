package com.seminario.primercorte.model;

import java.io.Serializable;

/**
 * Esta clase es requerida para crear un response (respuesta) el cual contiene el token JWT que
 * sera retornado al usuario.JwtResponse
 */
public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String tokenType;
    private final int idUsuario;
    private final String jwtToken;

    public JwtResponse(int idUsuario, String jwtToken) {
        this.tokenType = "Bearer";
        this.idUsuario = idUsuario;
        this.jwtToken = jwtToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getJwtToken() {
        return jwtToken;
    }
}
