package com.seminario.primercorte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimercorteApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrimercorteApplication.class, args);
    }

}
