package com.seminario.primercorte.service;

import java.util.List;

import com.seminario.primercorte.dto.CompraDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.seminario.primercorte.model.Compra;
import com.seminario.primercorte.repo.ICompra;

@RestController
@RequestMapping("rest/compra")
public class RepoCompra {

    @Autowired
    private ICompra repo;

    @GetMapping
    public List<Compra> listar() {
		return repo.findAll();
	}
	@PostMapping
	public void insertar(@RequestBody CompraDTO compraDto){

		Compra compra = new Compra();
		compra.setCuotasCompra(compraDto.getCuotasCompra());
		compra.setDetalleCompra(compraDto.getDetalleCompra());
		compra.setFechaCompra(compraDto.getFechaCompra());
		compra.setIdTarjeta(compraDto.getIdTarjeta());
		compra.setIdUsuario(compraDto.getIdUsuario());
		compra.setValorCompra(compraDto.getValorCompra());

		repo.save(compra);
	}
	
	@PutMapping
	public void modificar(@RequestBody Compra compra){

		/*Compra compra = new Compra();
		compra.setCuotasCompra(compraDto.getCuotasCompra());
		compra.setDetalleCompra(compraDto.getDetalleCompra());
		compra.setFechaCompra(compraDto.getFechaCompra());
		compra.setIdTarjeta(compraDto.getIdTarjeta());
		compra.setIdUsuario(compraDto.getIdUsuario());
		compra.setValorCompra(compraDto.getValorCompra());
		compra.setIdCompra(compraDto.getIdcompra());*/

		repo.save(compra);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id){
		repo.deleteById(id);
	}
}