package com.seminario.primercorte.service;

import java.util.List;
import java.util.Optional;

import com.seminario.primercorte.dto.TarjetaCreditoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.seminario.primercorte.model.TarjetaCredito;
import com.seminario.primercorte.repo.ITarjetaCreditoRepo;

@RestController
@RequestMapping("/tarjetacredito")
public class RepoTarjetaCredito {
	
	@Autowired
	public ITarjetaCreditoRepo repo;
	
	@GetMapping
	public List<TarjetaCredito> listar(){
		return repo.findAll();
	}
	
	@PostMapping
	public void insertar(@RequestBody TarjetaCredito tar){
		repo.save(tar);
	}

	public void insertar(@RequestBody TarjetaCreditoDTO tarjetaCreditoDTO){

		TarjetaCredito tarCre = new TarjetaCredito();
		tarCre.setBanco(tarjetaCreditoDTO.getBanco());
		tarCre.setCupo(tarjetaCreditoDTO.getCupo());
		tarCre.setFecha_corte(tarjetaCreditoDTO.getFecha_corte());
		tarCre.setNombre_alias(tarjetaCreditoDTO.getNombre_alias());
		tarCre.setTasa_interes(tarjetaCreditoDTO.getTasa_interes());

		repo.save(tarCre);
	}
	
	@PutMapping
	public void modificar(@RequestBody TarjetaCreditoDTO tarjetaCreditoDTO){

		TarjetaCredito tarCre = new TarjetaCredito();
		tarCre.setBanco(tarjetaCreditoDTO.getBanco());
		tarCre.setCupo(tarjetaCreditoDTO.getCupo());
		tarCre.setFecha_corte(tarjetaCreditoDTO.getFecha_corte());
		tarCre.setNombre_alias(tarjetaCreditoDTO.getNombre_alias());
		tarCre.setTasa_interes(tarjetaCreditoDTO.getTasa_interes());
		tarCre.setIdtarjeta(tarjetaCreditoDTO.getIdtarjeta());

		repo.save(tarCre);
		
	}
	
	@DeleteMapping(value = "/{idTarjeta}")
	public void eliminar(@PathVariable("idTarjeta") Integer id){
		repo.deleteById(id);
	}
}
