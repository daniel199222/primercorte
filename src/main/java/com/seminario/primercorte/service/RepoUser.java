package com.seminario.primercorte.service;

import com.seminario.primercorte.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.seminario.primercorte.model.User;
import com.seminario.primercorte.repo.IUsuarioRepo;

import java.util.Optional;

@RestController
@RequestMapping("rest/usuario")
public class RepoUser {
	@Autowired
	private IUsuarioRepo repo;

	@Autowired
	private PasswordEncoder encoder;

	@GetMapping(value = "/{id}")
	public Optional<User> listar(@PathVariable("id") Integer id){
		return repo.findByIdusuario(id);
	}

	@PostMapping
	public void insertar(@RequestBody UserDTO usrDTO){

		User usr = new User();
		usr.setNombre(usrDTO.getNombre());
		usr.setApellidos(usrDTO.getApellidos());
		usr.setIdentificacion(usrDTO.getIdentificacion());
		usr.setCorreo(usrDTO.getCorreo());
		usr.setContrasenia(usrDTO.getContrasenia());

		if(repo.existsByCorreo(usr.getCorreo())) {
			System.out.println("Usuario ya registrado listo");
			// return new ResponseEntity<String>("Fail -> Username is already taken!", HttpStatus.BAD_REQUEST);
		}

		int idUsuario = repo.maxIdusuario();
		int nextIdusuario = idUsuario + 1;

		usr.setContrasenia(encoder.encode(usr.getContrasenia()));
		// usr.setIdusuario(idUsuario);

		repo.save(usr);
	}

	@PutMapping
	public void modificar(@RequestBody UserDTO usrDTO){

		User usr = new User();
		usr.setNombre(usrDTO.getNombre());
		usr.setApellidos(usrDTO.getApellidos());
		usr.setIdentificacion(usrDTO.getIdentificacion());
		usr.setCorreo(usrDTO.getCorreo());
		usr.setContrasenia(usrDTO.getContrasenia());
		usr.setIdusuario(usrDTO.getIdusuario());

		repo.save(usr);
		
	
	}

	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id){
		repo.deleteById(id);
	}

}
