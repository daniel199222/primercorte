package com.seminario.primercorte.service;

import com.seminario.primercorte.configuracion.JwtTokenUtil;
import com.seminario.primercorte.dto.UserDTO;
import com.seminario.primercorte.model.JwtRequest;
import com.seminario.primercorte.model.JwtResponse;
import com.seminario.primercorte.model.User;
import com.seminario.primercorte.repo.IUsuarioRepo;
import com.seminario.primercorte.service.JwtUserDetailsService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Expone un API POST /autenticate utilizando este controlador. EL API POST obtiene el usuario y la contraseña
 * en el body. Utilizando el Spring Authentication Manager, se realizar la autenticacion utilizando el usuario y la contraseña.
 * Si las credenciales son validas, se retornara un token JWT utilizando la clase JWTokenUtil.
 */
@RestController
@CrossOrigin
@Api(value = "Authentication REST Endpoint")
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private IUsuarioRepo iUsuarioRepo;

    @Autowired
    private PasswordEncoder encoder;

    @RequestMapping(value = "/rest/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);

        User user = iUsuarioRepo.findByCorreo(authenticationRequest.getUsername())
                .orElseThrow(() ->
                        new UsernameNotFoundException("Usuario no econtrado: " + authenticationRequest.getUsername()));

        return ResponseEntity.ok(new JwtResponse(user.getIdusuario(), token));
    }

    @RequestMapping(value = "/rest/register", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody UserDTO usrDTO) throws Exception {

        User usr = new User();
        usr.setNombre(usrDTO.getNombre());
        usr.setApellidos(usrDTO.getApellidos());
        usr.setIdentificacion(usrDTO.getIdentificacion());
        usr.setCorreo(usrDTO.getCorreo());
        usr.setContrasenia(usrDTO.getContrasenia());

        if(iUsuarioRepo.existsByCorreo(usr.getCorreo())) {
            return new ResponseEntity<String>("Usuario ya registrado", HttpStatus.BAD_REQUEST);
        }
        usr.setContrasenia(encoder.encode(usr.getContrasenia()));
        iUsuarioRepo.save(usr);

        return ResponseEntity.ok(null);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        }catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
