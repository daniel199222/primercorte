package com.seminario.primercorte.service;

import com.seminario.primercorte.model.User;
import com.seminario.primercorte.model.UserPrinciple;
import com.seminario.primercorte.repo.IUsuarioRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    IUsuarioRepo usuarioRepo;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = usuarioRepo.findByCorreo(username)
                    .orElseThrow(() ->
                            new UsernameNotFoundException("Usuario no econtrado: " + username));

        return UserPrinciple.build(user);

        /*if ("prueba".equals(username)) {
            return new User("prueba", "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6", new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: "+username);
        }*/
    }
}
