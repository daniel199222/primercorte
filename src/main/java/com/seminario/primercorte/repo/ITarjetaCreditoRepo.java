package com.seminario.primercorte.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.seminario.primercorte.model.TarjetaCredito;


@Repository
public interface ITarjetaCreditoRepo extends JpaRepository<TarjetaCredito, Integer>{

    Optional<TarjetaCredito> findByIdtarjeta(int idtarjeta);

}
