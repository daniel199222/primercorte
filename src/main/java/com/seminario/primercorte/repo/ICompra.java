package com.seminario.primercorte.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.seminario.primercorte.model.Compra;
@Repository
public interface ICompra extends JpaRepository<Compra, Integer>{

}