package com.seminario.primercorte.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.seminario.primercorte.model.User;

import java.util.Optional;

@Repository
public interface IUsuarioRepo extends JpaRepository<User, Integer>{

    Optional<User> findByCorreo(String correo);
    Boolean existsByCorreo(String correo);

    Optional<User> findByIdusuario(int idUsuario);

    @Query("SELECT coalesce(max(a.idusuario), 0) FROM User a")
    int maxIdusuario();
}
